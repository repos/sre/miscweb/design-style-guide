**This project is now archived. See [T360362](https://phabricator.wikimedia.org/T360362).**

**Visit its improved and enhanced successor: [Codex](https://doc.wikimedia.org/codex/latest/)**

_Imported from [gerrit:design/style-guide](https://gerrit.wikimedia.org/r/plugins/gitiles/design/style-guide/) with latest commits pulled from [github:wikimedia/WikimediaUI-Style-Guide](https://github.com/wikimedia/WikimediaUI-Style-Guide)._

***

# Wikimedia Design Style Guide (WikimediaUI)

The Wikimedia Design Style Guide ensures a consistent look and interaction behavior for Wikimedia Movement products.

The style guide features unique focus areas like accessibility, internationalization and localization, originating from large-scale websites, diverse languages and communities like Wikipedia, Wikimedia Commons or Wikidata and software platforms like MediaWiki.

Its guidelines aim to help designers and developers with their Wikimedia projects, as part of the Foundation or in a volunteer capacity.

View the guide at [design.wikimedia.org/style-guide/](https://design.wikimedia.org/style-guide/).

## Contribute

We welcome and appreciate pull requests and feedback from everyone.
Please see [CONTRIBUTING.md](CONTRIBUTING.md).

## Download

Download of the style guide and all its resources is available at [GitHub](https://github.com/wikimedia/WikimediaUI-Style-Guide).


## Credits

The Wikimedia Design Style Guide is provided by [Wikimedia Foundation Design team](https://www.mediawiki.org/wiki/Design) and [contributors](https://github.com/wikimedia/WikimediaUI-Style-Guide/graphs/contributors).

## Development

## Blubber container image

Blubber images for design-style-guide page.

### Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`

### Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/design-style-guide:<timestamp>`

## Deploy changes

- Merge changes to master branch.

- Update image version on [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/helmfile.d/services/miscweb/values-design-style-guide.yaml#3) with [latest value](https://gitlab.wikimedia.org/repos/sre/miscweb/design-style-guide/-/jobs/). _The value is in the `#13` step in the job 'publish-image'._

- Add wmf-sre-collab (group) as reviewer and wait for merge.

- Deploy to production from the deployment server. 

See [WikiTech: Miscweb](https://wikitech.wikimedia.org/wiki/Miscweb#Deploy_to_Kubernets).
